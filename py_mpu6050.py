import smbus
import math

DFLT_BUS = smbus.SMBus(1)
DFLT_ADDR = 0x68 # via i2cdetect
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

def dist(a, b):
	return math.sqrt(a*a + b*b)

class MPU6050:
	def __init__(self,
				bus=DFLT_BUS,
				address=DFLT_ADDR):
		self.bus = bus
		self.address = address
		self.activate()

	def read_byte(self, reg):
		return self.bus.read_byte_data(self.address, reg)

	def read_word(self, reg):
		h = self.bus.read_byte_data(self.address, reg)
		l = self.bus.read_byte_data(self.address, reg+1)
		value = (h << 8) + l
		return value

	def read_word_2c(self, reg):
		val = self.read_word(reg)
		if (val >= 0x8000):
			return -((65535 - val) + 1)
		else:
			return val

	def get_y_rotation(self, x, y, z):
		radians = math.atan2(x, dist(y,z))
		return -math.degrees(radians)

	def get_x_rotation(self, x, y, z):
		radians = math.atan2(y, dist(x,z))
		return math.degrees(radians)

	def activate(self):
		self.bus.write_byte_data(self.address, power_mgmt_1, 0)

	def get_gyro_raw_data(self):
		gyro_x = self.read_word_2c(0x43)
		gyro_y = self.read_word_2c(0x45)
		gyro_z = self.read_word_2c(0x47)
		return [gyro_x, gyro_y, gyro_z]

	def get_acc_raw_data(self):
		acc_x = self.read_word_2c(0x3b)
		acc_y = self.read_word_2c(0x3d)
		acc_z = self.read_word_2c(0x3f)
		return [acc_x, acc_y, acc_z]

	def get_gyro_scaled_data(self):
		[gyro_x, gyro_y, gyro_z] = self.get_gyro_raw_data()
		scaled_factor = 131
		return [gyro_x / scaled_factor, gyro_y / scaled_factor, gyro_z / scaled_factor]

	def get_acc_scaled_data(self):
		[acc_x, acc_y, acc_z] = self.get_acc_raw_data()
		scaled_factor = 16384.0
		return [acc_x / scaled_factor, acc_y / scaled_factor, acc_z / scaled_factor]

	def get_rotation(self):
		[acc_x, acc_y, acc_z] = self.get_acc_scaled_data()
		return [self.get_x_rotation(acc_x, acc_y, acc_z), self.get_y_rotation(acc_x, acc_y, acc_z)]

