import py_qmc5883l
import py_mpu6050
import math
import coord_converter

def get_radec(lat, lon, jday): 
	sensor = py_qmc5883l.QMC5883L()
	m = sensor.get_magnet()
	deg = sensor.get_bearing()

	sensor2 = py_mpu6050.MPU6050()
	[rot_x, rot_y] = sensor2.get_rotation()

	az = coord_converter.deg_to_rad(deg)
	al = coord_converter.deg_to_rad(rot_x)

	print("alaz", al, az)

	[ra, dec] = coord_converter.azalt_to_radec(al, az, lat, lon, jday)

	print("radec", ra, dec)
	return ra, dec

if __name__ == '__main__':
	get_radec()

