#!/usr/bin/env python2
from math import sin, cos, tan, asin, acos, pi

def deg_to_rad(alpha):
	return alpha * pi / 180

def azalt_to_hadec_rad(a, A, phi): # al, az, phi rad, phi: latitude observer
	sin_delta = sin(a) * sin(phi) + cos(a) * cos(phi) * cos(A)
	delta = asin(sin_delta) # radian

	cos_H = (sin(a) - (sin(phi) * sin(delta))) / (cos(phi) * cos(delta))
	H = acos(cos_H) # radian
	H = 2 * pi - H if sin(A) > 0 else H

	return [H, delta]

def azalt_to_hadec_deg(al, az, phi):
	a = deg_to_rad(al)
	A = deg_to_rad(az)
	phi = deg_to_rad(phi)
	return azalt_to_hadec_rad(a, A, phi)

def azalt_to_radec_helper(al, az, phi, LST):
	[H, dec] = azalt_to_hadec_rad(al, az, phi)
	alpha = LST - H
	if alpha < -pi:
		alpha += 2 * pi
	if alpha > pi:
		alpha -= 2 * pi

	return [alpha, dec]

def jday_lon_2_lst(jday, lon):
	JD = int(jday - 0.5) + 0.5
	rem = jday - JD
	UT = rem *  24 # hour

	S = JD - 2451545.0
	T = S / 36525.0
	T0 = 6.697374558 + (2400.051336 * T) + (0.000025862 * (T ** 2))
	T0 = T0 % 24
	T0 = T0 + 24 if T0 < 0 else T0

	A = UT * 1.002737909 + T0

	A = A % 24
	GST = A

	GST_rad = deg_to_rad(GST * 15)
	lon_rad = deg_to_rad(lon)

	LST = (GST_rad + lon_rad) % (2 * pi)
	return LST

def azalt_to_radec(al, az, lat, lon, jday):
	phi = deg_to_rad(lat)
	LST = jday_lon_2_lst(jday, lon)

	print("LST", LST)

	return azalt_to_radec_helper(al, az, phi, LST)


