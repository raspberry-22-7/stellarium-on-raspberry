#!/usr/bin/env python2
import sys
import yaml
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler, FileModifiedEvent


OBSERVED_DEBUG_FILE = './pi_sensor_debug.yml'
with open(OBSERVED_DEBUG_FILE) as f:
    DEBUG_DICT = yaml.safe_load(f)


class DebugFileDummySensorEventHandler(FileSystemEventHandler):

    def on_modified(self, event):
        if event.src_path == OBSERVED_DEBUG_FILE:
            with open(OBSERVED_DEBUG_FILE) as f:
                DEBUG_DICT.update(yaml.safe_load(f))
                print 'Loaded new %s' % OBSERVED_DEBUG_FILE


path = sys.argv[1] if len(sys.argv) > 1 else '.'
event_handler = DebugFileDummySensorEventHandler()
observer = Observer()
observer.schedule(event_handler, path)
observer.start()


def get_radec():
    return float(DEBUG_DICT['ra']), float(DEBUG_DICT['dec'])


def main():
    try:
        while True:
            time.sleep(1)
            print get_radec()

    except KeyboardInterrupt:
        observer.stop()


def join():
    observer.join()


if __name__ == "__main__":
    main()
