#!/bin/bash

gpios=(16 26 5 6)

setup_gpio() {
    for i in "${gpios[@]}";
    do
        echo $i > /sys/class/gpio/export;
        echo "out" > "/sys/class/gpio/gpio$i/direction";
    done
}

teardown_gpio() {
    for i in "${gpios[@]}";
    do
        echo "0" > "/sys/class/gpio/gpio$i/value"; # Dunno if this is necessary, please check
        echo $i > /sys/class/gpio/unexport;
    done
}

if [ "$1" = '--help' ]; then
  echo "usage: $0 [--help | stop | -]"
elif [ "$1" = 'stop' ]; then
  teardown_gpio
else
  setup_gpio
fi
